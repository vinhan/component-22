<?php $pageid="home";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="p-home">
   <section class="p-home-1">
      <ul>
         <li><img src="assets/img/home/slide01.jpg" alt=""></li>
      </ul>
   </section>

   <section class="p-home-2">
      <div class="l-inner">
         <div class="p-home-2__wrap">
            <div class="p-home-2__box">
               <h3 class="p-home-2__box-title">Dummy</h3>

               <ul class="p-home-2__box-list">
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
               </ul>
            </div>

            <div class="p-home-2__box">
               <h3 class="p-home-2__box-title">Dummy</h3>

               <ul class="p-home-2__box-list">
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-2__box-img"><img src="assets/img/home/img01.jpg" alt=""></figure>
                     <h4 class="p-home-2__box-name">Dummy</h4>
                     <p class="p-home-2__box-times">000</p>
                  </li>
               </ul>
            </div>

            <p class="p-home-2__txt"><a href="#" class="u-text-link">Dummy</a></p>
         </div>
      </div>
   </section>

   <section class="p-home-3">
      <div class="l-inner">
         <div class="l-left">
            <div class="p-home-3-1-1">
               <h2 class="c-title-1">Dummy</h2>

               <ul class="p-home-3-1-1__list">
                  <li>
                     <figure class="p-home-3-1-1__img"><img src="assets/img/home/img02.jpg" alt=""></figure>
                     <h4 class="p-home-3-1-1__name">Dummy</h4>
                     <p class="p-home-3-1-1__times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-3-1-1__img"><img src="assets/img/home/img02.jpg" alt=""></figure>
                     <h4 class="p-home-3-1-1__name">Dummy</h4>
                     <p class="p-home-3-1-1__times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-3-1-1__img"><img src="assets/img/home/img02.jpg" alt=""></figure>
                     <h4 class="p-home-3-1-1__name">Dummy</h4>
                     <p class="p-home-3-1-1__times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-3-1-1__img"><img src="assets/img/home/img02.jpg" alt=""></figure>
                     <h4 class="p-home-3-1-1__name">Dummy</h4>
                     <p class="p-home-3-1-1__times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-3-1-1__img"><img src="assets/img/home/img02.jpg" alt=""></figure>
                     <h4 class="p-home-3-1-1__name">Dummy</h4>
                     <p class="p-home-3-1-1__times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-3-1-1__img"><img src="assets/img/home/img02.jpg" alt=""></figure>
                     <h4 class="p-home-3-1-1__name">Dummy</h4>
                     <p class="p-home-3-1-1__times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-3-1-1__img"><img src="assets/img/home/img02.jpg" alt=""></figure>
                     <h4 class="p-home-3-1-1__name">Dummy</h4>
                     <p class="p-home-3-1-1__times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-3-1-1__img"><img src="assets/img/home/img02.jpg" alt=""></figure>
                     <h4 class="p-home-3-1-1__name">Dummy</h4>
                     <p class="p-home-3-1-1__times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-3-1-1__img"><img src="assets/img/home/img02.jpg" alt=""></figure>
                     <h4 class="p-home-3-1-1__name">Dummy</h4>
                     <p class="p-home-3-1-1__times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-3-1-1__img"><img src="assets/img/home/img02.jpg" alt=""></figure>
                     <h4 class="p-home-3-1-1__name">Dummy</h4>
                     <p class="p-home-3-1-1__times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-3-1-1__img"><img src="assets/img/home/img02.jpg" alt=""></figure>
                     <h4 class="p-home-3-1-1__name">Dummy</h4>
                     <p class="p-home-3-1-1__times">000</p>
                  </li>
                  <li>
                     <figure class="p-home-3-1-1__img"><img src="assets/img/home/img02.jpg" alt=""></figure>
                     <h4 class="p-home-3-1-1__name">Dummy</h4>
                     <p class="p-home-3-1-1__times">000</p>
                  </li>
               </ul>

               <p class="p-home-3-1-1__txt"><a href="#" class="u-text-link">Dummy</a></p>
            </div>

            <div class="p-home-3-1-2">
               <h2 class="c-title-1">Dummy</h2>

               <div class="p-home-3-1-2__content">
                  <ul class="p-home-3-1-2__list p-home-3-1-2__listLeft">
                     <li class="p-home-3-1-2__item">
                        <span class="p-home-3-1-2__label">Dummy</span>

                        <ul class="p-home-3-1-2__listChild">
                           <li><a href="#">Dummy</a></li>
                        </ul>
                     </li>

                     <li class="p-home-3-1-2__item">
                        <span class="p-home-3-1-2__label">Dummy</span>

                        <ul class="p-home-3-1-2__listChild">
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                        </ul>
                     </li>

                     <li class="p-home-3-1-2__item">
                        <span class="p-home-3-1-2__label">Dummy</span>

                        <ul class="p-home-3-1-2__listChild">
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                        </ul>
                     </li>

                     <li class="p-home-3-1-2__item">
                        <span class="p-home-3-1-2__label">Dummy</span>

                        <ul class="p-home-3-1-2__listChild">
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                        </ul>
                     </li>

                     <li class="p-home-3-1-2__item">
                        <span class="p-home-3-1-2__label">Dummy</span>

                        <ul class="p-home-3-1-2__listChild">
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                        </ul>
                     </li>

                     <li class="p-home-3-1-2__item">
                        <span class="p-home-3-1-2__label">Dummy</span>

                        <ul class="p-home-3-1-2__listChild">
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                        </ul>
                     </li>
                  </ul>

                  <ul class="p-home-3-1-2__list p-home-3-1-2__listRight">
                     <li class="p-home-3-1-2__item">
                        <span class="p-home-3-1-2__label">Dummy</span>

                        <ul class="p-home-3-1-2__listChild">
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                        </ul>
                     </li>

                     <li class="p-home-3-1-2__item">
                        <span class="p-home-3-1-2__label">Dummy</span>

                        <ul class="p-home-3-1-2__listChild">
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                        </ul>
                     </li>

                     <li class="p-home-3-1-2__item">
                        <span class="p-home-3-1-2__label">Dummy</span>

                        <ul class="p-home-3-1-2__listChild">
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                        </ul>
                     </li>

                     <li class="p-home-3-1-2__item">
                        <span class="p-home-3-1-2__label">Dummy</span>

                        <ul class="p-home-3-1-2__listChild">
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                        </ul>
                     </li>

                     <li class="p-home-3-1-2__item">
                        <span class="p-home-3-1-2__label">Dummy</span>

                        <ul class="p-home-3-1-2__listChild">
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                        </ul>
                     </li>

                     <li class="p-home-3-1-2__item">
                        <span class="p-home-3-1-2__label">Dummy</span>

                        <ul class="p-home-3-1-2__listChild">
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                           <li><a href="#">Dummy</a></li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </div>

            <div class="p-home-3-1-3">
               <h2 class="c-title-1">Dummy</h2>

               <ul class="p-home-3-1-3__columns fixHeight">
                  <li class="p-home-3-1-3__item">
                     <div class="p-home-3-1-3__border fixHeightChild">
                        <h4 class="p-home-3-1-3__name">Dummy</h4>

                        <ul class="p-home-3-1-3__list">
                           <li>Dummy</li>
                           <li>Dummy</li>
                           <li>Dummy</li>
                           <li>Dummy</li>
                           <li>Dummy</li>
                           <li>Dummy</li>
                        </ul>
                     </div>
                  </li>

                  <li class="p-home-3-1-3__item">
                     <div class="p-home-3-1-3__border fixHeightChild">
                        <h4 class="p-home-3-1-3__name">Dummy</h4>

                        <ul class="p-home-3-1-3__list">
                           <li>Dummy</li>
                           <li>Dummy</li>
                           <li>Dummy</li>
                           <li>Dummy</li>
                           <li>Dummy</li>
                           <li>Dummy</li>
                        </ul>
                     </div>
                  </li>

                  <li class="p-home-3-1-3__item">
                     <div class="p-home-3-1-3__border fixHeightChild">
                        <h4 class="p-home-3-1-3__name">Dummy</h4>

                        <ul class="p-home-3-1-3__list">
                           <li>Dummy</li>
                           <li>Dummy</li>
                           <li>Dummy</li>
                           <li>Dummy</li>
                           <li>Dummy</li>
                           <li>Dummy</li>
                           <li>Dummy</li>
                        </ul>
                     </div>
                  </li>
               </ul>
            </div>

            <div class="p-home-3-1-4">
               <h2 class="c-title-1">Dummy</h2>

               <div class="p-home-3-1-4__listTab">
                  <div class="c-list-6">
                     <ul class="c-list-6__list">
                        <li class="active"><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                     </ul>
                  </div>

                  <div class="p-home-3-1-4__content">
                     <div class="p-home-3-1-4__tabContent active">
                        <ul class="c-list-3 p-home-3-1-4__slide">
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                        </ul>
                     </div>
                     <div class="p-home-3-1-4__tabContent">
                        <ul class="c-list-3 p-home-3-1-4__slide">
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="c-list-3__frame">
                                 <figure class="c-list-3__img">
                                    <img src="/assets/img/demo.jpg" alt="list">
                                    <span class="c-list-3__icon">1</span>
                                 </figure>
                                 <div class="c-list-3__content">
                                    <span class="c-list-3__style">Mini</span>
                                    <h4 class="c-list-3__name">MINICooper</h4>
                                    <div class="c-list-3__price">
                                       0 ~ 0 <span>dummy</span>
                                    </div>
                                 </div>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>

            <div class="p-home-3-1-5">
               <h2 class="c-title-1">Dummy</h2>

               <div class="p-home-3-1-5__listTab">
                  <div class="c-list-6">
                     <ul class="c-list-6__list">
                        <li class="active"><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                        <li><a href="#">Dummy</a></li>
                     </ul>
                  </div>

                  <div class="p-home-3-1-5__content">
                     <div class="p-home-3-1-5__tabContent active">
                        <ul class="p-home-3-1-5__slide">
                           <li>
                              <a href="#" class="p-home-3-1-5__frame">
                                 <div class="p-home-3-1-5__pd">
                                    <figure class="p-home-3-1-5__img">
                                       <img src="/assets/img/demo.jpg" alt="list">
                                    </figure>
                                    <h4 class="p-home-3-1-5__name">Dummy</h4>
                                 </div>
                                 <ul class="p-home-3-1-5__gPrice">
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                 </ul>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="p-home-3-1-5__frame">
                                 <div class="p-home-3-1-5__pd">
                                    <figure class="p-home-3-1-5__img">
                                       <img src="/assets/img/demo.jpg" alt="list">
                                    </figure>
                                    <h4 class="p-home-3-1-5__name">Dummy</h4>
                                 </div>
                                 <ul class="p-home-3-1-5__gPrice">
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                 </ul>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="p-home-3-1-5__frame">
                                 <div class="p-home-3-1-5__pd">
                                    <figure class="p-home-3-1-5__img">
                                       <img src="/assets/img/demo.jpg" alt="list">
                                    </figure>
                                    <h4 class="p-home-3-1-5__name">Dummy</h4>
                                 </div>
                                 <ul class="p-home-3-1-5__gPrice">
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                 </ul>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="p-home-3-1-5__frame">
                                 <div class="p-home-3-1-5__pd">
                                    <figure class="p-home-3-1-5__img">
                                       <img src="/assets/img/demo.jpg" alt="list">
                                    </figure>
                                    <h4 class="p-home-3-1-5__name">Dummy</h4>
                                 </div>
                                 <ul class="p-home-3-1-5__gPrice">
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                 </ul>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="p-home-3-1-5__frame">
                                 <div class="p-home-3-1-5__pd">
                                    <figure class="p-home-3-1-5__img">
                                       <img src="/assets/img/demo.jpg" alt="list">
                                    </figure>
                                    <h4 class="p-home-3-1-5__name">Dummy</h4>
                                 </div>
                                 <ul class="p-home-3-1-5__gPrice">
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                 </ul>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="p-home-3-1-5__frame">
                                 <div class="p-home-3-1-5__pd">
                                    <figure class="p-home-3-1-5__img">
                                       <img src="/assets/img/demo.jpg" alt="list">
                                    </figure>
                                    <h4 class="p-home-3-1-5__name">Dummy</h4>
                                 </div>
                                 <ul class="p-home-3-1-5__gPrice">
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                 </ul>
                              </a>
                           </li>
                           <li>
                              <a href="#" class="p-home-3-1-5__frame">
                                 <div class="p-home-3-1-5__pd">
                                    <figure class="p-home-3-1-5__img">
                                       <img src="/assets/img/demo.jpg" alt="list">
                                    </figure>
                                    <h4 class="p-home-3-1-5__name">Dummy</h4>
                                 </div>
                                 <ul class="p-home-3-1-5__gPrice">
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                    <li>
                                       <p class="p-home-3-1-5__gPrice-title">Dummy</p>
                                       <p class="p-home-3-1-5__gPrice-price">00.0<span>dummy</span></p>
                                    </li>
                                 </ul>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>

            <div class="p-home-3-1-6">
               <h2 class="c-title-1">Dummy</h2>

               <ul class="c-list-2">
                  <li>
                     <a href="#" class="c-list-2__frame">
                        <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-2__content">
                           <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#" class="c-list-2__frame">
                        <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-2__content">
                           <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#" class="c-list-2__frame">
                        <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-2__content">
                           <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#" class="c-list-2__frame">
                        <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-2__content">
                           <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#" class="c-list-2__frame">
                        <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-2__content">
                           <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#" class="c-list-2__frame">
                        <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-2__content">
                           <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
               </ul>
            </div>

            <div class="p-home-3-1-7">
               <h2 class="c-title-1">Dummy</h2>

               <div class="p-home-3-1-7__blockTxt">
                  <h3 class="c-title-2">Dummy</h3>
                  <div class="p-home-3-1-7__txt">
                     <p>Dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy 
                     dummy dummy dummy dummy</p>
                     <p>Dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy</p>
                     <p>Dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy</p>
                  </div>
               </div>

               <div class="p-home-3-1-7__blockTxt">
                  <h3 class="c-title-2">Dummy</h3>
                  <div class="p-home-3-1-7__txt">
                     <p>Dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy 
                     dummy dummy dummy dummy</p>
                     <p>Dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy</p>
                     <p>Dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy</p>
                  </div>
               </div>
            </div>
         </div>

         <aside class="l-right">
            <div class="p-home-3-2-1">
               <a href="#"><img src="assets/img/home/banner.jpg" alt=""></a>
            </div>

            <div class="p-home-3-2-2">
               <h3 class="c-title-2">Dummy</h3>

               <ul class="c-list-1">
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
               </ul>
            </div>

            <div class="p-home-3-2-3">
               <a href="#"><img src="assets/img/home/banner.jpg" alt=""></a>
            </div>

            <div class="p-home-3-2-4">
               <h3 class="c-title-2">Dummy</h3>

               <ul class="c-list-1">
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
                  <li>
                     <a href="#">
                        <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                        <div class="c-list-1__content">
                           <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
                        </div>
                     </a>
                  </li>
               </ul>
            </div>

            <div class="p-home-3-2-5">
               <img src="assets/img/demo_2.jpg" alt="">
            </div>
         </aside>
      </div>
   </section>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>
