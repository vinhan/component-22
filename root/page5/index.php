<?php $pageid="page5";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header.php'); ?>

<div class="l-inner">
   <nav class="c-breadcrumb">
      <ul>
         <li><a href="#">dummy</a></li>
         <li><a href="#">dummy</a></li>
         <li><a href="#">dummy</a></li>
         <li>dummy</li>
      </ul>
   </nav>

   <div class="p-page-5">
      <h2 class="p-page-5__title">Dummy <span>Dummy</span></h2>

      <div class="p-page-5-1">
         <form class="p-page-5-1__filter">
            <h3 class="p-page-5-1__title">Dummy</h3>

            <ul class="p-page-5-1__rowHead">
               <li>
                  <span class="p-page-5-1__rowHead-title">Dummy</span>

                  <div class="c-select p-page-5-1__rowHead-select1">
                     <select>
                        <option value="">dummy</option>
                        <option value="">dummy</option>
                     </select>
                  </div>
               </li>
               <li>
                  <span class="p-page-5-1__rowHead-title">Dummy</span>

                  <div class="c-select p-page-5-1__rowHead-select2">
                     <select>
                        <option value="">dummy</option>
                        <option value="">dummy</option>
                     </select>
                  </div>
               </li>
               <li>
                  <span class="p-page-5-1__rowHead-title">Dummy</span>

                  <div class="c-select p-page-5-1__rowHead-select3">
                     <select>
                        <option value="">dummy</option>
                        <option value="">dummy</option>
                     </select>
                  </div>
               </li>
            </ul>

            <div class="p-page-5-1__rowMid">
               <ul class="p-page-5-1__rowMid-left">
                  <li class="p-page-5-1__rowMid-leftItem">
                     <span class="p-page-5-1__rowMid-title">Dummy</span>
                     <div class="p-page-5-1__rowMid-content">
                        <ul class="p-page-5-1__rowMid-list">
                           <li>
                              <div class="c-select p-page-5-1__rowMid-select">
                                 <select>
                                    <option value="">dummy</option>
                                    <option value="">dummy</option>
                                 </select>
                              </div>
                              <span class="p-page-5-1__rowMid-icon">~</span>
                              <div class="c-select p-page-5-1__rowMid-select">
                                 <select>
                                    <option value="">dummy</option>
                                    <option value="">dummy</option>
                                 </select>
                              </div>
                           </li>
                           <li>
                              <label class="c-radiobox p-page-5-1__rowMid-radio1">dummy
                                 <input type="radio" name="radio" checked="checked">
                                 <span class="c-radiobox__mark"></span>
                              </label>

                              <label class="c-radiobox p-page-5-1__rowMid-radio1">dummy
                                 <input type="radio" name="radio">
                                 <span class="c-radiobox__mark"></span>
                              </label>
                           </li>
                           <li>
                              <label class="c-checkbox">dummy
                                 <input type="checkbox" checked="checked">
                                 <span class="c-checkbox__mark"></span>
                              </label>
                           </li>
                        </ul>
                     </div>
                  </li>

                  <li class="p-page-5-1__rowMid-leftItem">
                     <span class="p-page-5-1__rowMid-title">Dummy</span>

                     <div class="p-page-5-1__rowMid-content">
                        <div class="p-page-5-1__rowMid-dbSeclect">
                           <div class="c-select p-page-5-1__rowMid-select">
                              <select>
                                 <option value="">dummy</option>
                                 <option value="">dummy</option>
                              </select>
                           </div>
                           <span class="p-page-5-1__rowMid-icon">~</span>
                           <div class="c-select p-page-5-1__rowMid-select">
                              <select>
                                 <option value="">dummy</option>
                                 <option value="">dummy</option>
                              </select>
                           </div>
                        </div>
                     </div>
                  </li>

                  <li class="p-page-5-1__rowMid-leftItem">
                     <span class="p-page-5-1__rowMid-title">Dummy</span>

                     <div class="p-page-5-1__rowMid-content">
                        <div class="p-page-5-1__rowMid-dbSeclect">
                           <div class="c-select p-page-5-1__rowMid-select">
                              <select>
                                 <option value="">dummy</option>
                                 <option value="">dummy</option>
                              </select>
                           </div>
                           <span class="p-page-5-1__rowMid-icon">~</span>
                           <div class="c-select p-page-5-1__rowMid-select">
                              <select>
                                 <option value="">dummy</option>
                                 <option value="">dummy</option>
                              </select>
                           </div>
                        </div>
                     </div>
                  </li>

                  <li class="p-page-5-1__rowMid-leftItem">
                     <span class="p-page-5-1__rowMid-title">Dummy</span>

                     <div class="p-page-5-1__rowMid-content">
                        <div class="c-select p-page-5-1__rowMid-select">
                           <select>
                              <option value="">dummy</option>
                              <option value="">dummy</option>
                           </select>
                        </div>
                     </div>
                  </li>
               </ul>

               <ul class="p-page-5-1__rowMid-right">
                  <li class="p-page-5-1__rowMid-rightItem">
                     <span class="p-page-5-1__rowMid-title">Dummy</span>

                     <div class="p-page-5-1__rowMid-content">
                        <a href="#" class="c-link-label p-page-5-1__rowMid-lb">Dummy</a>
                     </div>
                  </li>

                  <li class="p-page-5-1__rowMid-rightItem">
                     <span class="p-page-5-1__rowMid-title">Dummy</span>

                     <div class="p-page-5-1__rowMid-content">
                        <div class="p-page-5-1__rowMid-gRadio">
                           <label class="c-radiobox p-page-5-1__rowMid-radio2">dummy
                              <input type="radio" name="radio1" checked="checked">
                              <span class="c-radiobox__mark"></span>
                           </label>

                           <label class="c-radiobox p-page-5-1__rowMid-radio2">dummy
                              <input type="radio" name="radio1">
                              <span class="c-radiobox__mark"></span>
                           </label>

                           <label class="c-radiobox p-page-5-1__rowMid-radio2">dummy
                              <input type="radio" name="radio1">
                              <span class="c-radiobox__mark"></span>
                           </label>
                        </div>
                     </div>
                  </li>

                  <li class="p-page-5-1__rowMid-rightItem">
                     <span class="p-page-5-1__rowMid-title">Dummy</span>

                     <div class="p-page-5-1__rowMid-content">
                        <div class="p-page-5-1__rowMid-gRadio">
                           <label class="c-radiobox p-page-5-1__rowMid-radio2">dummy
                              <input type="radio" name="radio2" checked="checked">
                              <span class="c-radiobox__mark"></span>
                           </label>

                           <label class="c-radiobox p-page-5-1__rowMid-radio2">dummy
                              <input type="radio" name="radio2">
                              <span class="c-radiobox__mark"></span>
                           </label>

                           <label class="c-radiobox p-page-5-1__rowMid-radio2">dummy
                              <input type="radio" name="radio2">
                              <span class="c-radiobox__mark"></span>
                           </label>
                        </div>
                     </div>
                  </li>

                  <li class="p-page-5-1__rowMid-rightItem">
                     <span class="p-page-5-1__rowMid-title">Dummy</span>

                     <div class="p-page-5-1__rowMid-content">
                        <ul class="p-page-5-1__rowMid-colors">
                           <li><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color1"></span></li>
                           <li><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color2"></span></li>
                           <li class="active"><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color3"></span></li>
                           <li><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color4"></span></li>
                           <li><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color5"></span></li>
                           <li><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color6"></span></li>
                           <li><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color7"></span></li>
                           <li><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color8"></span></li>
                           <li><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color9"></span></li>
                           <li><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color10"></span></li>
                           <li><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color11"></span></li>
                           <li><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color12"></span></li>
                           <li><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color13"></span></li>
                           <li><span class="p-page-5-1__rowMid-color p-page-5-1__rowMid-color14"></span></li>
                        </ul>
                     </div>
                  </li>

                  <li class="p-page-5-1__rowMid-rightItem">
                     <span class="p-page-5-1__rowMid-title">Dummy</span>

                     <div class="p-page-5-1__rowMid-content">
                        <ul class="p-page-5-1__rowMid-checkboxs">
                           <li>
                              <label class="c-checkbox">dummy
                                 <input type="checkbox" checked="checked">
                                 <span class="c-checkbox__mark"></span>
                              </label>
                              <label class="c-checkbox">dummy
                                 <input type="checkbox" checked="checked">
                                 <span class="c-checkbox__mark"></span>
                              </label>
                              <label class="c-checkbox">dummy
                                 <input type="checkbox" checked="checked">
                                 <span class="c-checkbox__mark"></span>
                              </label>
                           </li>
                           <li>
                              <label class="c-checkbox">dummy
                                 <input type="checkbox" checked="checked">
                                 <span class="c-checkbox__mark"></span>
                              </label>
                              <label class="c-checkbox">dummy
                                 <input type="checkbox" checked="checked">
                                 <span class="c-checkbox__mark"></span>
                              </label>
                              <label class="c-checkbox">dummy
                                 <input type="checkbox" checked="checked">
                                 <span class="c-checkbox__mark"></span>
                              </label>
                           </li>
                        </ul>
                     </div>
                  </li>
               </ul>
            </div>

            <div class="p-page-5-1__rowBot">
               <a href="#" class="u-text-link">Dummy</a>

               <div class="p-page-5-1__rowBot-box">
                  <a href="#" class="p-page-5-1__rowBot-boxLink c-link-label">Dummy</a>
                  <p class="p-page-5-1__rowBot-boxTxt">Dummy dummy <span>000</span>dummy</p>
                  <button class="c-button-1" type="button">Dummy</button>
               </div>
            </div>
         </form>
      </div>

      <div class="p-page-5-2">
         <div class="c-list-4">
            <ul class="c-list-4__tab">
               <li class="active"><a href="#">dummy</a></li>
               <li><a href="#">dummy</a></li>
               <li><a href="#">dummy</a></li>
               <li><a href="#">dummy</a></li>
               <li><a href="#">dummy</a></li>
               <li><a href="#">dummy</a></li>
            </ul>

            <div class="c-list-4__countBox">
               <span>dummy :</span>

               <ul class="c-list-4__countBox--list">
                  <li class="active"><span>40</span></li>
                  <li><a href="#">80</a></li>
                  <li><a href="#">120</a></li>
               </ul>
            </div>
         </div>

         <div class="c-txt-1">
            <span>000</span> dummy  <span>0~00</span> dummy
         </div>

         <ul class="c-list-5">
            <li class="c-list-5__item">
               <div class="c-list-5__intro">
                  <figure class="c-list-5__intro--img"><img src="/assets/img/demo_1.jpg" alt=""></figure>

                  <div class="c-list-5__intro--content">
                     <p class="c-list-5__intro--brand">dummy</p>
                     <h4 class="c-list-5__intro--name">Dummy Dummy Dummy Dummy </h4>
                     <div class="c-list-5__intro--desc">dummy dummy dummy dummy dummy dummy dummy dummy dummy...</div>
                     <ul class="c-list-5__intro--listNavi">
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                     </ul>
                     <ul class="c-list-5__intro--listPrice">
                        <li>
                           <p class="c-list-5__intro--titPrice">車両本体価格</p>
                           <p class="c-list-5__intro--price">00.0<span>dummy</span></p>
                        </li>

                        <li>
                           <p class="c-list-5__intro--titPrice">支払総額</p>
                           <p class="c-list-5__intro--price">000<span>dummy</span></p>
                        </li>
                     </ul>
                  </div>
               </div>

               <div class="c-list-5__param">
                  <ul class="c-list-5__param--lists">
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li class="c-list-5__param--address">
                        <span class="c-list-5__param--name">dummy</span>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                  </ul>

                  <div class="c-list-5__param--txt">
                     <span>dummy</span>
                     Dummy dummy dummy
                  </div>

                  <div class="c-list-5__param--buttons">
                     <a href="#" class="c-button-2">Dummy</a>
                     <a href="#" class="c-button-1">Dummy</a>
                  </div>
               </div>
            </li>

            <li class="c-list-5__item">
               <div class="c-list-5__intro">
                  <figure class="c-list-5__intro--img"><img src="/assets/img/demo_1.jpg" alt=""></figure>

                  <div class="c-list-5__intro--content">
                     <p class="c-list-5__intro--brand">dummy</p>
                     <h4 class="c-list-5__intro--name">Dummy Dummy Dummy Dummy </h4>
                     <div class="c-list-5__intro--desc">dummy dummy dummy dummy dummy dummy dummy dummy dummy...</div>
                     <ul class="c-list-5__intro--listNavi">
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                     </ul>
                     <ul class="c-list-5__intro--listPrice">
                        <li>
                           <p class="c-list-5__intro--titPrice">車両本体価格</p>
                           <p class="c-list-5__intro--price">00.0<span>dummy</span></p>
                        </li>

                        <li>
                           <p class="c-list-5__intro--titPrice">支払総額</p>
                           <p class="c-list-5__intro--price">000<span>dummy</span></p>
                        </li>
                     </ul>
                  </div>
               </div>

               <div class="c-list-5__param">
                  <ul class="c-list-5__param--lists">
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li class="c-list-5__param--address">
                        <span class="c-list-5__param--name">dummy</span>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                  </ul>

                  <div class="c-list-5__param--txt">
                     <span>dummy</span>
                     Dummy dummy dummy
                  </div>

                  <div class="c-list-5__param--buttons">
                     <a href="#" class="c-button-2">Dummy</a>
                     <a href="#" class="c-button-1">Dummy</a>
                  </div>
               </div>
            </li>

            <li class="c-list-5__item">
               <div class="c-list-5__intro">
                  <figure class="c-list-5__intro--img"><img src="/assets/img/demo_1.jpg" alt=""></figure>

                  <div class="c-list-5__intro--content">
                     <p class="c-list-5__intro--brand">dummy</p>
                     <h4 class="c-list-5__intro--name">Dummy Dummy Dummy Dummy </h4>
                     <div class="c-list-5__intro--desc">dummy dummy dummy dummy dummy dummy dummy dummy dummy...</div>
                     <ul class="c-list-5__intro--listNavi">
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                     </ul>
                     <ul class="c-list-5__intro--listPrice">
                        <li>
                           <p class="c-list-5__intro--titPrice">車両本体価格</p>
                           <p class="c-list-5__intro--price">00.0<span>dummy</span></p>
                        </li>

                        <li>
                           <p class="c-list-5__intro--titPrice">支払総額</p>
                           <p class="c-list-5__intro--price">000<span>dummy</span></p>
                        </li>
                     </ul>
                  </div>
               </div>

               <div class="c-list-5__param">
                  <ul class="c-list-5__param--lists">
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li class="c-list-5__param--address">
                        <span class="c-list-5__param--name">dummy</span>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                  </ul>

                  <div class="c-list-5__param--txt">
                     <span>dummy</span>
                     Dummy dummy dummy
                  </div>

                  <div class="c-list-5__param--buttons">
                     <a href="#" class="c-button-2">Dummy</a>
                     <a href="#" class="c-button-1">Dummy</a>
                  </div>
               </div>
            </li>

            <li class="c-list-5__item">
               <div class="c-list-5__intro">
                  <figure class="c-list-5__intro--img"><img src="/assets/img/demo_1.jpg" alt=""></figure>

                  <div class="c-list-5__intro--content">
                     <p class="c-list-5__intro--brand">dummy</p>
                     <h4 class="c-list-5__intro--name">Dummy Dummy Dummy Dummy </h4>
                     <div class="c-list-5__intro--desc">dummy dummy dummy dummy dummy dummy dummy dummy dummy...</div>
                     <ul class="c-list-5__intro--listNavi">
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                     </ul>
                     <ul class="c-list-5__intro--listPrice">
                        <li>
                           <p class="c-list-5__intro--titPrice">車両本体価格</p>
                           <p class="c-list-5__intro--price">00.0<span>dummy</span></p>
                        </li>

                        <li>
                           <p class="c-list-5__intro--titPrice">支払総額</p>
                           <p class="c-list-5__intro--price">000<span>dummy</span></p>
                        </li>
                     </ul>
                  </div>
               </div>

               <div class="c-list-5__param">
                  <ul class="c-list-5__param--lists">
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li class="c-list-5__param--address">
                        <span class="c-list-5__param--name">dummy</span>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                  </ul>

                  <div class="c-list-5__param--txt">
                     <span>dummy</span>
                     Dummy dummy dummy
                  </div>

                  <div class="c-list-5__param--buttons">
                     <a href="#" class="c-button-2">Dummy</a>
                     <a href="#" class="c-button-1">Dummy</a>
                  </div>
               </div>
            </li>

            <li class="c-list-5__item">
               <div class="c-list-5__intro">
                  <figure class="c-list-5__intro--img"><img src="/assets/img/demo_1.jpg" alt=""></figure>

                  <div class="c-list-5__intro--content">
                     <p class="c-list-5__intro--brand">dummy</p>
                     <h4 class="c-list-5__intro--name">Dummy Dummy Dummy Dummy </h4>
                     <div class="c-list-5__intro--desc">dummy dummy dummy dummy dummy dummy dummy dummy dummy...</div>
                     <ul class="c-list-5__intro--listNavi">
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                     </ul>
                     <ul class="c-list-5__intro--listPrice">
                        <li>
                           <p class="c-list-5__intro--titPrice">車両本体価格</p>
                           <p class="c-list-5__intro--price">00.0<span>dummy</span></p>
                        </li>

                        <li>
                           <p class="c-list-5__intro--titPrice">支払総額</p>
                           <p class="c-list-5__intro--price">000<span>dummy</span></p>
                        </li>
                     </ul>
                  </div>
               </div>

               <div class="c-list-5__param">
                  <ul class="c-list-5__param--lists">
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li class="c-list-5__param--address">
                        <span class="c-list-5__param--name">dummy</span>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                  </ul>

                  <div class="c-list-5__param--txt">
                     <span>dummy</span>
                     Dummy dummy dummy
                  </div>

                  <div class="c-list-5__param--buttons">
                     <a href="#" class="c-button-2">Dummy</a>
                     <a href="#" class="c-button-1">Dummy</a>
                  </div>
               </div>
            </li>

            <li class="c-list-5__item">
               <div class="c-list-5__intro">
                  <figure class="c-list-5__intro--img"><img src="/assets/img/demo_1.jpg" alt=""></figure>

                  <div class="c-list-5__intro--content">
                     <p class="c-list-5__intro--brand">dummy</p>
                     <h4 class="c-list-5__intro--name">Dummy Dummy Dummy Dummy </h4>
                     <div class="c-list-5__intro--desc">dummy dummy dummy dummy dummy dummy dummy dummy dummy...</div>
                     <ul class="c-list-5__intro--listNavi">
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                     </ul>
                     <ul class="c-list-5__intro--listPrice">
                        <li>
                           <p class="c-list-5__intro--titPrice">車両本体価格</p>
                           <p class="c-list-5__intro--price">00.0<span>dummy</span></p>
                        </li>

                        <li>
                           <p class="c-list-5__intro--titPrice">支払総額</p>
                           <p class="c-list-5__intro--price">000<span>dummy</span></p>
                        </li>
                     </ul>
                  </div>
               </div>

               <div class="c-list-5__param">
                  <ul class="c-list-5__param--lists">
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li class="c-list-5__param--address">
                        <span class="c-list-5__param--name">dummy</span>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                  </ul>

                  <div class="c-list-5__param--txt">
                     <span>dummy</span>
                     Dummy dummy dummy
                  </div>

                  <div class="c-list-5__param--buttons">
                     <a href="#" class="c-button-2">Dummy</a>
                     <a href="#" class="c-button-1">Dummy</a>
                  </div>
               </div>
            </li>

            <li class="c-list-5__item">
               <div class="c-list-5__intro">
                  <figure class="c-list-5__intro--img"><img src="/assets/img/demo_1.jpg" alt=""></figure>

                  <div class="c-list-5__intro--content">
                     <p class="c-list-5__intro--brand">dummy</p>
                     <h4 class="c-list-5__intro--name">Dummy Dummy Dummy Dummy </h4>
                     <div class="c-list-5__intro--desc">dummy dummy dummy dummy dummy dummy dummy dummy dummy...</div>
                     <ul class="c-list-5__intro--listNavi">
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                     </ul>
                     <ul class="c-list-5__intro--listPrice">
                        <li>
                           <p class="c-list-5__intro--titPrice">車両本体価格</p>
                           <p class="c-list-5__intro--price">00.0<span>dummy</span></p>
                        </li>

                        <li>
                           <p class="c-list-5__intro--titPrice">支払総額</p>
                           <p class="c-list-5__intro--price">000<span>dummy</span></p>
                        </li>
                     </ul>
                  </div>
               </div>

               <div class="c-list-5__param">
                  <ul class="c-list-5__param--lists">
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li class="c-list-5__param--address">
                        <span class="c-list-5__param--name">dummy</span>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                  </ul>

                  <div class="c-list-5__param--txt">
                     <span>dummy</span>
                     Dummy dummy dummy
                  </div>

                  <div class="c-list-5__param--buttons">
                     <a href="#" class="c-button-2">Dummy</a>
                     <a href="#" class="c-button-1">Dummy</a>
                  </div>
               </div>
            </li>

            <li class="c-list-5__item">
               <div class="c-list-5__intro">
                  <figure class="c-list-5__intro--img"><img src="/assets/img/demo_1.jpg" alt=""></figure>

                  <div class="c-list-5__intro--content">
                     <p class="c-list-5__intro--brand">dummy</p>
                     <h4 class="c-list-5__intro--name">Dummy Dummy Dummy Dummy </h4>
                     <div class="c-list-5__intro--desc">dummy dummy dummy dummy dummy dummy dummy dummy dummy...</div>
                     <ul class="c-list-5__intro--listNavi">
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                     </ul>
                     <ul class="c-list-5__intro--listPrice">
                        <li>
                           <p class="c-list-5__intro--titPrice">車両本体価格</p>
                           <p class="c-list-5__intro--price">00.0<span>dummy</span></p>
                        </li>

                        <li>
                           <p class="c-list-5__intro--titPrice">支払総額</p>
                           <p class="c-list-5__intro--price">000<span>dummy</span></p>
                        </li>
                     </ul>
                  </div>
               </div>

               <div class="c-list-5__param">
                  <ul class="c-list-5__param--lists">
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li class="c-list-5__param--address">
                        <span class="c-list-5__param--name">dummy</span>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                  </ul>

                  <div class="c-list-5__param--txt">
                     <span>dummy</span>
                     Dummy dummy dummy
                  </div>

                  <div class="c-list-5__param--buttons">
                     <a href="#" class="c-button-2">Dummy</a>
                     <a href="#" class="c-button-1">Dummy</a>
                  </div>
               </div>
            </li>

            <li class="c-list-5__item">
               <div class="c-list-5__intro">
                  <figure class="c-list-5__intro--img"><img src="/assets/img/demo_1.jpg" alt=""></figure>

                  <div class="c-list-5__intro--content">
                     <p class="c-list-5__intro--brand">dummy</p>
                     <h4 class="c-list-5__intro--name">Dummy Dummy Dummy Dummy </h4>
                     <div class="c-list-5__intro--desc">dummy dummy dummy dummy dummy dummy dummy dummy dummy...</div>
                     <ul class="c-list-5__intro--listNavi">
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                        <li>Dummy</li>
                     </ul>
                     <ul class="c-list-5__intro--listPrice">
                        <li>
                           <p class="c-list-5__intro--titPrice">車両本体価格</p>
                           <p class="c-list-5__intro--price">00.0<span>dummy</span></p>
                        </li>

                        <li>
                           <p class="c-list-5__intro--titPrice">支払総額</p>
                           <p class="c-list-5__intro--price">000<span>dummy</span></p>
                        </li>
                     </ul>
                  </div>
               </div>

               <div class="c-list-5__param">
                  <ul class="c-list-5__param--lists">
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li>
                        <span class="c-list-5__param--name">dummy</span>
                        <p class="c-list-5__param--number">0000</p>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                     <li class="c-list-5__param--address">
                        <span class="c-list-5__param--name">dummy</span>
                        <span class="c-list-5__param--character">dummy</span>
                     </li>
                  </ul>

                  <div class="c-list-5__param--txt">
                     <span>dummy</span>
                     Dummy dummy dummy
                  </div>

                  <div class="c-list-5__param--buttons">
                     <a href="#" class="c-button-2">Dummy</a>
                     <a href="#" class="c-button-1">Dummy</a>
                  </div>
               </div>
            </li>
         </ul>

         <nav class="c-pagi">
            <ul>
               <li class="c-pagi__prev"><a href="#"><</a></li>
               <li><a href="#">1</a></li>
               <li><a href="#">2</a></li>
               <li class="c-pagi__active"><span>3</span></li>
               <li><a href="#">4</a></li>
               <li class="c-pagi__dot"><span>...</span></li>
               <li><a href="#">8</a></li>
               <li class="c-pagi__next"><a href="#">></a></li>
            </ul>
         </nav>
      </div>

      <div class="p-page-5-3">
         <h3 class="c-title-2">Dummy</h3>

         <ul class="c-list-3">
            <li>
               <a href="#" class="c-list-3__frame">
                  <figure class="c-list-3__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                  <div class="c-list-3__content">
                     <span class="c-list-3__style">Mini</span>
                     <h4 class="c-list-3__name">MINICooper</h4>
                     <div class="c-list-3__price">
                        0 ~ 0 <span>dummy</span>
                     </div>
                  </div>
               </a>
            </li>
            <li>
               <a href="#" class="c-list-3__frame">
                  <figure class="c-list-3__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                  <div class="c-list-3__content">
                     <span class="c-list-3__style">Mini</span>
                     <h4 class="c-list-3__name">MINICooper</h4>
                     <div class="c-list-3__price">
                        0 ~ 0 <span>dummy</span>
                     </div>
                  </div>
               </a>
            </li>
            <li>
               <a href="#" class="c-list-3__frame">
                  <figure class="c-list-3__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                  <div class="c-list-3__content">
                     <span class="c-list-3__style">Mini</span>
                     <h4 class="c-list-3__name">MINICooper</h4>
                     <div class="c-list-3__price">
                        0 ~ 0 <span>dummy</span>
                     </div>
                  </div>
               </a>
            </li>
            <li>
               <a href="#" class="c-list-3__frame">
                  <figure class="c-list-3__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                  <div class="c-list-3__content">
                     <span class="c-list-3__style">Mini</span>
                     <h4 class="c-list-3__name">MINICooper</h4>
                     <div class="c-list-3__price">
                        0 ~ 0 <span>dummy</span>
                     </div>
                  </div>
               </a>
            </li>
            <li>
               <a href="#" class="c-list-3__frame">
                  <figure class="c-list-3__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                  <div class="c-list-3__content">
                     <span class="c-list-3__style">Mini</span>
                     <h4 class="c-list-3__name">MINICooper</h4>
                     <div class="c-list-3__price">
                        0 ~ 0 <span>dummy</span>
                     </div>
                  </div>
               </a>
            </li>
            <li>
               <a href="#" class="c-list-3__frame">
                  <figure class="c-list-3__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                  <div class="c-list-3__content">
                     <span class="c-list-3__style">Mini</span>
                     <h4 class="c-list-3__name">MINICooper</h4>
                     <div class="c-list-3__price">
                        0 ~ 0 <span>dummy</span>
                     </div>
                  </div>
               </a>
            </li>
            <li>
               <a href="#" class="c-list-3__frame">
                  <figure class="c-list-3__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                  <div class="c-list-3__content">
                     <span class="c-list-3__style">Mini</span>
                     <h4 class="c-list-3__name">MINICooper</h4>
                     <div class="c-list-3__price">
                        0 ~ 0 <span>dummy</span>
                     </div>
                  </div>
               </a>
            </li>
         </ul>
      </div>

      <div class="p-page-5-4">
         <h3 class="c-title-1">Dummy</h3>

         <div class="p-page-5-4__blockTxt">
            <h4 class="c-title-2">Dummy</h4>

            <div class="p-page-5-4__blockTxt-content">
               <p>Dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy</p>
            </div>
         </div>

         <div class="p-page-5-4__blockTxt">
            <h4 class="c-title-2">Dummy</h4>

            <div class="p-page-5-4__blockTxt-content">
               <p>Dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy</p>
               <p>Dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy</p>
            </div>
         </div>
      </div>

      <div class="p-page-5-5">
         <h3 class="c-title-1">Dummy</h3>

         <ul class="c-list-2">
            <li>
               <a href="#" class="c-list-2__frame">
                  <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                  <div class="c-list-2__content">
                     <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
                  </div>
               </a>
            </li>
            <li>
               <a href="#" class="c-list-2__frame">
                  <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                  <div class="c-list-2__content">
                     <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
                  </div>
               </a>
            </li>
            <li>
               <a href="#" class="c-list-2__frame">
                  <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                  <div class="c-list-2__content">
                     <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
                  </div>
               </a>
            </li>
            <li>
               <a href="#" class="c-list-2__frame">
                  <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                  <div class="c-list-2__content">
                     <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
                  </div>
               </a>
            </li>
            <li>
               <a href="#" class="c-list-2__frame">
                  <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                  <div class="c-list-2__content">
                     <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
                  </div>
               </a>
            </li>
            <li>
               <a href="#" class="c-list-2__frame">
                  <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
                  <div class="c-list-2__content">
                     <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
                  </div>
               </a>
            </li>
         </ul>
      </div>
   </div>
</div>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer.php'); ?>