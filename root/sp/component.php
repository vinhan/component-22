<?php $pageid="allcomponent";?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/header_sp.php'); ?>

<?php /*========================================
breadcrumb
================================================*/ ?>
<div class="c-dev-title1">breadcrumb</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/breadcrumb_sp.php'); ?>

<?php /*========================================
component
================================================*/ ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/01_btn.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/02_title.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/03_icon.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/04_form.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/05_text.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/06_navi.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/07_img.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/08_list.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/09_table.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/10_line.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/11_video.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/12_slide.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/13_tab.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/component/sp/99_other.php'); ?>

<?php /*========================================
side
================================================*/ ?>
<div class="c-dev-title1">side</div>
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/side_sp.php'); ?>

<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/footer_sp.php'); ?>