<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include('meta_sp.php'); ?>
<link href="/assets/css/style_sp.css" rel="stylesheet">
<script src="/assets/js/jquery-3.3.1.min.js"></script>
<script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
</head>
<body class="page-<?php echo $pageid; ?>">
   <header class="c-header">
      <div class="c-header__menu">
         <span class="c-header__menu-icon">
            <i></i>
            <i></i>
            <i></i>
         </span>
         <p class="c-header__menu-txt">MENU</p>
      </div>

      <h1 class="c-header__logo">
         <a href="">
            <img src="/assets/img/common_sp/logo.jpg" alt="">
            <span>dummy</span>
         </a>
      </h1>

      <a href="#" class="c-header__wishlist">
         <img src="/assets/img/common_sp/icon_heart.jpg" alt="">
         <span>dummy</span>
      </a>
   </header>