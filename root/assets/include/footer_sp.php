      <footer class="c-footer">
         <ul class="c-footer__top">
            <li><a href="#">Dummy</a></li>
            <li><a href="#">Dummy</a></li>
            <li><a href="#">Dummy</a></li>
            <li><a href="#">Dummy</a></li>
         </ul>

         <div class="c-footer__bot">
            <figure class="c-footer__bot-logo"><a href="#"><img src="/assets/img/common_sp/logo_foot.jpg" alt="MOBY"></a></figure>

            <p class="c-footer__bot-txt">Dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy</p>

            <p class="c-footer__bot-copyright">dummy dummy dummy dummy dummy dummy</p>
         </div>
      </footer>

      <script src="/assets/js/slick.min.js"></script>
      <script src="/assets/js/functions_sp.js"></script>

      <script type="text/javascript">
         $(document).ready(function() {
            
         });
      </script>
   </body>
</html>