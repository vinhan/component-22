      <footer class="c-footer">
         <div class="l-inner">
            <ul class="c-footer__columns">
               <li class="c-footer__columns--1">
                  <figure class="c-footer__logo">
                     <a href="#"><img src="assets/img/common/logo_footer.jpg" alt="MOBY"></a>
                  </figure>

                  <p class="c-footer__txt">dummy dummy dummy dummy dummy dummy </p>

                  <figure class="c-footer__brand">
                     <a href="#"><img src="assets/img/common/brand.jpg" alt="brand"></a>
                  </figure>
               </li>

               <li class="c-footer__columns--2">
                  
               </li>

               <li class="c-footer__columns--3">
                  
               </li>
            </ul>

            <div class="c-footer__wrapMenu">
               <ul class="c-footer__menuLeft">
                  <li><a href="#">dummy</a></li>
                  <li><a href="#">dummy</a></li>
               </ul>

               <ul class="c-footer__menuRight">
                  <li><a href="#">dummy</a></li>
                  <li><a href="#">dummy</a></li>
                  <li><a href="#">dummy</a></li>
                  <li><a href="#" class="c-footer__frameLink">dummy</a></li>
               </ul>
            </div>

            <div class="u-center"><a href="#" class="c-footer__frameLink">dumy</a></div>

            <p class="c-footer__copyright">2016-2019 DM SOLUTION Co.,Ltd</p>
         </div>
      </footer>

      <script src="/assets/js/fixHeight.js"></script>
      <script src="/assets/js/slick.min.js"></script>
      <script src="/assets/js/functions.js"></script>

      <script type="text/javascript">
         $(document).ready(function() {
            $('.p-home-3-1-4__slide').slick({
               infinite: true,
               slidesToShow: 5,
               arrows: false,
            });
            $('.p-home-3-1-5__slide').slick({
               infinite: true,
               slidesToShow: 5,
               arrows: false,
            });
         });
      </script>
   </body>
</html>