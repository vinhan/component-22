<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include('meta.php'); ?>
<link href="/assets/css/slick.css" rel="stylesheet">
<link href="/assets/css/style.css" rel="stylesheet">
<script src="/assets/js/jquery-3.3.1.min.js"></script>
<script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
</head>
<body class="page-<?php echo $pageid; ?>">
   <header class="c-header">
      <div class="l-inner">
         <h1 class="c-header__logo"><a href="./"><img src="assets/img/common/logo.jpg" alt="MOBY"></a></h1>

         <div class="c-header__cart">
            <a href="#">dummy<span>(3)</span></a>
         </div>
      </div>
   </header>

   <nav class="c-menu">
      <ul class="l-inner">
         <li><a href="#">dummy</a></li>
         <li><a href="#">dummy</a></li>
         <li><a href="#">dummy</a></li>
         <li><a href="#">dummy</a></li>
         <li><a href="#">dummy</a></li>
         <li><a href="#">dummy</a></li>
      </ul>
   </nav>