<?php /*========================================
title
================================================*/ ?>
<div class="c-dev-title1">Tab</div>

<?php /*----------------------------------------*/ ?>
<div class="l-inner">
   <div class="c-tab-1">
      <ul class="c-tab-1__tab">
         <li class="active"><a href="#">dummy</a></li>
         <li><a href="#">dummy</a></li>
         <li><a href="#">dummy</a></li>
         <li><a href="#">dummy</a></li>
         <li><a href="#">dummy</a></li>
         <li><a href="#">dummy</a></li>
      </ul>

      <div class="c-tab-1__countBox">
         <span>dummy :</span>

         <ul class="c-tab-1__countBox--list">
            <li class="active"><span>40</span></li>
            <li><a href="#">80</a></li>
            <li><a href="#">120</a></li>
         </ul>
      </div>
   </div>
</div>

<hr>

<div class="l-left">
   <div class="c-tab-2">
      <ul class="c-tab-2__list">
         <li class="active"><a href="#">Dummy</a></li>
         <li><a href="#">Dummy</a></li>
         <li><a href="#">Dummy</a></li>
         <li><a href="#">Dummy</a></li>
         <li><a href="#">Dummy</a></li>
         <li><a href="#">Dummy</a></li>
         <li><a href="#">Dummy</a></li>
         <li><a href="#">Dummy</a></li>
         <li><a href="#">Dummy</a></li>
         <li><a href="#">Dummy</a></li>
      </ul>
   </div>
</div>