<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>

<ul class="c-list-1">
   <li class="c-list-1__item">
      <a href="#">
         <figure class="c-list-1__item-img"><img src="/assets/img/home_sp/img04.jpg" alt=""></figure>
         <div class="c-list-1__item-content">
            <h4 class="c-list-1__item-name">Dummy dummy dummy dummy dummy</h4>
         </div>
      </a>
   </li>
   <li class="c-list-1__item">
      <a href="#">
         <figure class="c-list-1__item-img"><img src="/assets/img/home_sp/img04.jpg" alt=""></figure>
         <div class="c-list-1__item-content">
            <h4 class="c-list-1__item-name">Dummy dummy dummy dummy dummy</h4>
         </div>
      </a>
   </li>
</ul>

<hr>

<ul class="c-list-2">
   <li class="c-list-2__item">
      <a href="#">
         <figure class="c-list-2__item-img">
            <img src="/assets/img/home_sp/img02.jpg" alt="">
            <span class="c-list-2__item-icon">1</span>
         </figure>
         <div class="c-list-2__item-content">
            <p class="c-list-2__item-brand">Dummy</p>
            <h4 class="c-list-2__item-name">Dummy dummy dummy dummy dummy</h4>
            <p class="c-list-2__item-price">0~00<span>dummy</span></p>
         </div>
      </a>
   </li>
   <li class="c-list-2__item">
      <a href="#">
         <figure class="c-list-2__item-img">
            <img src="/assets/img/home_sp/img02.jpg" alt="">
            <span class="c-list-2__item-icon">2</span>
         </figure>
         <div class="c-list-2__item-content">
            <p class="c-list-2__item-brand">Dummy</p>
            <h4 class="c-list-2__item-name">Dummy dummy dummy dummy dummy</h4>
            <p class="c-list-2__item-price">0~00<span>dummy</span></p>
         </div>
      </a>
   </li>
</ul>