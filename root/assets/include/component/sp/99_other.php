<?php /*========================================
other
================================================*/ ?>
<div class="c-dev-title1">other</div>

<?php /*----------------------------------------*/ ?>
<div class="c-select-1">
   <select class="c-select-1__select">
      <option value="">dummy</option>
      <option value="">dummy</option>
   </select>
</div>

<hr>

<ul class="c-other-1">
   <li class="c-other-1__item">
      <div class="c-other-1__item-heading">
         <h4 class="c-other-1__item-name">Dummy</h4>
         <a href="#" class="c-other-1__item-icon"><img src="/assets/img/common_sp/icon.jpg" alt=""></a>
      </div>

      <div class="c-other-1__item-content">
         <figure class="c-other-1__item-img"><img src="/assets/img/home_sp/img05.jpg" alt=""></figure>

         <ul class="c-other-1__item-info">
            <li class="c-other-1__item-infoGprice">
               <span class="c-other-1__item-infoTit">dummy</span>

               <p class="c-other-1__item-infoPrice">000.0<span>dummy</span></p>
            </li>
            <li>
               <ul class="c-other-1__item-infoChild">
                  <li>
                     <span class="c-other-1__item-infoTit">dummy</span>

                     <p class="c-other-1__item">dummy</p>
                  </li>
                  <li>
                     <span class="c-other-1__item-infoTit">dummy</span>

                     <p class="c-other-1__item">dummy</p>
                  </li>
               </ul>
            </li>
            <li>
               <ul class="c-other-1__item-infoChild">
                  <li>
                     <span class="c-other-1__item-infoTit">dummy</span>

                     <p class="c-other-1__item-infotTxt">dummy</p>
                  </li>
                  <li>
                     <span class="c-other-1__item-infoTit">dummy</span>

                     <p class="c-other-1__item-infotTxt">dummy</p>
                  </li>
               </ul>
            </li>
            <li>
               <ul class="c-other-1__item-infoChild">
                  <li>
                     <span class="c-other-1__item-infoTit">dummy</span>

                     <p class="c-other-1__item-infotTxt">dummy</p>
                  </li>
                  <li>
                     <span class="c-other-1__item-infoTit">dummy</span>

                     <p class="c-other-1__item-infotTxt">dummy</p>
                  </li>
               </ul>
            </li>
            <li>
               <span class="c-other-1__item-infoTit">dummy</span>

               <p class="c-other-1__item-infotTxt">dummy dummy</p>
            </li>
         </ul>
      </div>

      <div class="c-other-1__item-desc">Dummy dummy dummy dummy dummy dummy dummy dummy dummy dummy</div>

      <a href="#" class="c-button-1">Dummy</a>
   </li>
</ul>