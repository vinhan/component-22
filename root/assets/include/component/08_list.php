<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="l-right">
   <ul class="c-list-1">
      <li>
         <a href="#">
            <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
            <div class="c-list-1__content">
               <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
            </div>
         </a>
      </li>
      <li>
         <a href="#">
            <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
            <div class="c-list-1__content">
               <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
            </div>
         </a>
      </li>
      <li>
         <a href="#">
            <figure class="c-list-1__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
            <div class="c-list-1__content">
               <p class="c-list-1__desc">dummy dummy dummy dummy dummy dummy</p>
            </div>
         </a>
      </li>
   </ul>
</div>

<hr>

<div class="l-inner">
   <ul class="c-list-2">
      <li>
         <a href="#" class="c-list-2__frame">
            <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
            <div class="c-list-2__content">
               <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
            </div>
         </a>
      </li>
      <li>
         <a href="#" class="c-list-2__frame">
            <figure class="c-list-2__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
            <div class="c-list-2__content">
               <p class="c-list-2__desc">dummy dummy dummy dummy dummy dummy</p>
            </div>
         </a>
      </li>
   </ul>
</div>

<hr>

<div class="l-inner">
   <ul class="c-list-3">
      <li>
         <a href="#" class="c-list-3__frame">
            <figure class="c-list-3__img">
               <img src="/assets/img/demo.jpg" alt="list">
               <span class="c-list-3__icon">1</span>
            </figure>
            <div class="c-list-3__content">
               <span class="c-list-3__style">Mini</span>
               <h4 class="c-list-3__name">MINICooper</h4>
               <div class="c-list-3__price">
                  0 ~ 0 <span>dummy</span>
               </div>
            </div>
         </a>
      </li>
      <li>
         <a href="#" class="c-list-3__frame">
            <figure class="c-list-3__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
            <div class="c-list-3__content">
               <span class="c-list-3__style">Mini</span>
               <h4 class="c-list-3__name">MINICooper</h4>
               <div class="c-list-3__price">
                  0 ~ 0 <span>dummy</span>
               </div>
            </div>
         </a>
      </li>
      <li>
         <a href="#" class="c-list-3__frame">
            <figure class="c-list-3__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
            <div class="c-list-3__content">
               <span class="c-list-3__style">Mini</span>
               <h4 class="c-list-3__name">MINICooper</h4>
               <div class="c-list-3__price">
                  0 ~ 0 <span>dummy</span>
               </div>
            </div>
         </a>
      </li>
      <li>
         <a href="#" class="c-list-3__frame">
            <figure class="c-list-3__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
            <div class="c-list-3__content">
               <span class="c-list-3__style">Mini</span>
               <h4 class="c-list-3__name">MINICooper</h4>
               <div class="c-list-3__price">
                  0 ~ 0 <span>dummy</span>
               </div>
            </div>
         </a>
      </li>
      <li>
         <a href="#" class="c-list-3__frame">
            <figure class="c-list-3__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
            <div class="c-list-3__content">
               <span class="c-list-3__style">Mini</span>
               <h4 class="c-list-3__name">MINICooper</h4>
               <div class="c-list-3__price">
                  0 ~ 0 <span>dummy</span>
               </div>
            </div>
         </a>
      </li>
      <li>
         <a href="#" class="c-list-3__frame">
            <figure class="c-list-3__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
            <div class="c-list-3__content">
               <span class="c-list-3__style">Mini</span>
               <h4 class="c-list-3__name">MINICooper</h4>
               <div class="c-list-3__price">
                  0 ~ 0 <span>dummy</span>
               </div>
            </div>
         </a>
      </li>
      <li>
         <a href="#" class="c-list-3__frame">
            <figure class="c-list-3__img"><img src="/assets/img/demo.jpg" alt="list"></figure>
            <div class="c-list-3__content">
               <span class="c-list-3__style">Mini</span>
               <h4 class="c-list-3__name">MINICooper</h4>
               <div class="c-list-3__price">
                  0 ~ 0 <span>dummy</span>
               </div>
            </div>
         </a>
      </li>
   </ul>
</div>