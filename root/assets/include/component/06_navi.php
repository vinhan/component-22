<?php /*========================================
navi
================================================*/ ?>
<div class="c-dev-title1">navi</div>

<?php /*----------------------------------------*/ ?>

<nav class="c-menu">
   <ul class="l-inner">
      <li><a href="#">dummy</a></li>
      <li><a href="#">dummy</a></li>
      <li><a href="#">dummy</a></li>
      <li><a href="#">dummy</a></li>
      <li><a href="#">dummy</a></li>
      <li><a href="#">dummy</a></li>
   </ul>
</nav>

<hr>

<nav class="c-pagi">
   <ul>
      <li class="c-pagi__prev"><a href="#"><</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li class="c-pagi__active"><span>3</span></li>
      <li><a href="#">4</a></li>
      <li class="c-pagi__dot"><span>...</span></li>
      <li><a href="#">8</a></li>
      <li class="c-pagi__next"><a href="#">></a></li>
   </ul>
</nav>