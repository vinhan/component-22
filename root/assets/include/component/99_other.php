<?php /*========================================
other
================================================*/ ?>
<div class="c-dev-title1">other</div>

<?php /*----------------------------------------*/ ?>
<div class="c-select">
   <select>
      <option value="">dummy</option>
      <option value="">dummy</option>
   </select>
</div>

<hr>

<label class="c-checkbox">dummy
   <input type="checkbox" checked="checked">
   <span class="c-checkbox__mark"></span>
</label>

<label class="c-checkbox">dummy
   <input type="checkbox">
   <span class="c-checkbox__mark"></span>
</label>

<hr>

<label class="c-radiobox">dummy
   <input type="radio" name="radio" checked="checked">
   <span class="c-radiobox__mark"></span>
</label>

<label class="c-radiobox">dummy
   <input type="radio" name="radio">
   <span class="c-radiobox__mark"></span>
</label>

<hr>

<a href="#" class="c-link-label">dummy</a>

<hr>

<div class="l-inner">
   <ul class="c-other-1">
      <li class="c-other-1__item">
         <div class="c-other-1__intro">
            <figure class="c-other-1__intro--img"><img src="/assets/img/demo_1.jpg" alt=""></figure>

            <div class="c-other-1__intro--content">
               <p class="c-other-1__intro--brand">dummy</p>
               <h4 class="c-other-1__intro--name">Dummy Dummy Dummy Dummy </h4>
               <div class="c-other-1__intro--desc">dummy dummy dummy dummy dummy dummy dummy dummy dummy...</div>
               <ul class="c-other-1__intro--listNavi">
                  <li>Dummy</li>
                  <li>Dummy</li>
                  <li>Dummy</li>
                  <li>Dummy</li>
               </ul>
               <ul class="c-other-1__intro--listPrice">
                  <li>
                     <p class="c-other-1__intro--titPrice">車両本体価格</p>
                     <p class="c-other-1__intro--price">00.0<span>dummy</span></p>
                  </li>

                  <li>
                     <p class="c-other-1__intro--titPrice">支払総額</p>
                     <p class="c-other-1__intro--price">000<span>dummy</span></p>
                  </li>
               </ul>
            </div>
         </div>

         <div class="c-other-1__param">
            <ul class="c-other-1__param--lists">
               <li>
                  <span class="c-other-1__param--name">dummy</span>
                  <p class="c-other-1__param--number">0000</p>
                  <span class="c-other-1__param--character">dummy</span>
               </li>
               <li>
                  <span class="c-other-1__param--name">dummy</span>
                  <p class="c-other-1__param--number">0000</p>
                  <span class="c-other-1__param--character">dummy</span>
               </li>
               <li>
                  <span class="c-other-1__param--name">dummy</span>
                  <p class="c-other-1__param--number">0000</p>
                  <span class="c-other-1__param--character">dummy</span>
               </li>
               <li>
                  <span class="c-other-1__param--name">dummy</span>
                  <p class="c-other-1__param--number">0000</p>
                  <span class="c-other-1__param--character">dummy</span>
               </li>
               <li>
                  <span class="c-other-1__param--name">dummy</span>
                  <p class="c-other-1__param--number">0000</p>
                  <span class="c-other-1__param--character">dummy</span>
               </li>
               <li>
                  <span class="c-other-1__param--name">dummy</span>
                  <p class="c-other-1__param--number">0000</p>
                  <span class="c-other-1__param--character">dummy</span>
               </li>
               <li class="c-other-1__param--address">
                  <span class="c-other-1__param--name">dummy</span>
                  <span class="c-other-1__param--character">dummy</span>
               </li>
            </ul>

            <div class="c-other-1__param--txt">
               <span>dummy</span>
               Dummy dummy dummy
            </div>

            <div class="c-other-1__param--buttons">
               <a href="#" class="c-button-2">Dummy</a>
               <a href="#" class="c-button-1">Dummy</a>
            </div>
         </div>
      </li>
   </ul>
</div>